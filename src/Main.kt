import com.fazecast.jSerialComm.SerialPort
import java.awt.FlowLayout
import java.awt.event.ActionEvent
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import java.io.OutputStreamWriter
import java.lang.Exception
import java.lang.StringBuilder
import java.util.*
import javax.swing.*
import kotlin.math.abs

const val baudRate: Int = 115200
const val dataBits: Int = 8
const val stopBit: Int = 1

const val motorForward: Char = '1'
const val motorOff: Char = '0'
const val motorBack: Char = '2'

const val numberOfMotors = 4

val portsList = mutableListOf<SerialPort>()
val portsModel = DefaultComboBoxModel<String>()

val jFrame = JFrame("Ball Catcher Controller")
val jPanel = JPanel(FlowLayout())
val jCheckBoxsPanel = JPanel().also { it.layout = BoxLayout(it, BoxLayout.Y_AXIS) }
val portsJComboBox = JComboBox(portsModel)
val refreshButton = JButton("Refresh")
val motorsCheckBox = Array(numberOfMotors) { JCheckBox("motor $it", false) }
val startAllButton = JButton("start all")
val stopAllButton = JButton("stop all")

var selectedIndex = -1

val portsSelectedListener = { e: ActionEvent ->
    selectedIndex = portsJComboBox.selectedIndex

    if (selectedIndex == -1) {
        showSelectPortError()
    } else {
        if (openPort(selectedIndex)) {
        }
    }

    jFrame.requestFocus()
}

fun main(args: Array<String>) {
    initFrame()
}

fun openPort(index: Int): Boolean {
    val port = portsList[index]

    port.baudRate = baudRate
    port.numDataBits = dataBits
    port.numStopBits = stopBit

    if(!port.openPort()) {
        updatePortsComboBox()
        showPortNotOpendError()

        return false
    }

    return true
}

fun sendMotorStatus(a: Char, b: Char, c: Char, d: Char) {
    //portsList[selectedIndex].openPort()
    //portsList[selectedIndex].setComPortPar
    var data: Int = 0

    data += if (a != motorOff) 0b00000001 else 0
    data += if (a != motorForward) 0b00000010 else 0

    data += if (b != motorOff) 0b00000100 else 0
    data += if (b != motorForward) 0b00001000 else 0

    data += if (c != motorOff) 0b00010000 else 0
    data += if (c != motorForward) 0b00100000 else 0

    data += if (d != motorOff) 0b01000000 else 0
    data += if (d != motorForward) 0b10000000 else 0

    val byte = data.toByte()

    portsList[selectedIndex].outputStream.apply {
        write(ByteArray(1) { byte })
        flush()
    }
    //println("send: $send, ${try {"answer: ${inputScanner.nextLine()}"} catch (e: Exception) { "error: ${e.message}" } }")
}

fun initFrame() {
    updatePortsComboBox()

    jPanel.add(portsJComboBox)
    jPanel.add(refreshButton)
    refreshButton.addActionListener {
        updatePortsComboBox()
    }

    motorsCheckBox.forEachIndexed { index, checkBox ->
        checkBox.addActionListener {
            if (selectedIndex == -1) {
                checkBox.isSelected = false
                showSelectPortError()
            } else {
                //sendMotorStatus(index, if(checkBox.isSelected) motorForward else motorOff)
            }
        }
    }

    startAllButton.addActionListener {
        if (selectedIndex == -1) {
            showSelectPortError()
        } else {
        }
    }

    stopAllButton.addActionListener {
        if (selectedIndex == -1) {
            showSelectPortError()
        } else {
        }
    }

    //jCheckBoxsPanel.add(startAllButton)
    //jCheckBoxsPanel.add(stopAllButton)

    jFrame.addKeyListener(object: KeyListener {
        override fun keyTyped(e: KeyEvent?) {
        }

        override fun keyPressed(e: KeyEvent?) {
            when (e!!.keyCode) {
                KeyEvent.VK_NUMPAD1 -> {
                    println("1 pressed")
                    sendMotorStatus(
                        motorBack,
                        motorForward,
                        motorForward,
                        motorBack)
                }
                KeyEvent.VK_NUMPAD2 -> {
                    println("2 pressed")
                    sendMotorStatus(
                        motorBack,
                        motorOff,
                        motorForward,
                        motorOff)
                }
                KeyEvent.VK_NUMPAD3 -> {
                    println("3 pressed")
                    sendMotorStatus(
                        motorBack,
                        motorBack,
                        motorForward,
                        motorForward)
                }
                KeyEvent.VK_NUMPAD4 -> {
                    println("4 pressed")
                    sendMotorStatus(
                        motorOff,
                        motorForward,
                        motorOff,
                        motorBack)
                }
                KeyEvent.VK_NUMPAD5 -> {
                    println("5 pressed")
                    sendMotorStatus(
                        motorOff,
                        motorOff,
                        motorOff,
                        motorOff)

                }
                KeyEvent.VK_NUMPAD6 -> {
                    println("6 pressed")
                    sendMotorStatus(
                        motorOff,
                        motorBack,
                        motorOff,
                        motorForward)

                }
                KeyEvent.VK_NUMPAD7 -> {
                    println("7 pressed")
                    sendMotorStatus(
                        motorForward,
                        motorForward,
                        motorBack,
                        motorBack)

                }
                KeyEvent.VK_NUMPAD8 -> {
                    println("8 pressed")
                    sendMotorStatus(
                        motorForward,
                        motorOff,
                        motorBack,
                        motorOff)
                }
                KeyEvent.VK_NUMPAD9 -> {
                    println("9 pressed")
                    sendMotorStatus(
                        motorForward,
                        motorBack,
                        motorBack,
                        motorForward)
                }

                KeyEvent.VK_DIVIDE -> {
                    println("divide pressed")
                    sendMotorStatus(
                        motorBack,
                        motorBack,
                        motorBack,
                        motorBack)
                }
                KeyEvent.VK_MULTIPLY -> {
                    println("multiply pressed")
                    sendMotorStatus(
                        motorForward,
                        motorForward,
                        motorForward,
                        motorForward)
                }
            }

        }

        override fun keyReleased(e: KeyEvent?) {

        }

    })

    jFrame.add(jPanel)
    jFrame.setSize(300, 300)
    jFrame.setLocationRelativeTo(null)
    jFrame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
    jFrame.isVisible = true
    jFrame.isFocusable = true
}

fun updatePortsComboBox() {
    removeAllActionsListeners(portsJComboBox)

    portsList.apply {
        clear()
        addAll(SerialPort.getCommPorts())
    }

    portsModel.apply {
        removeAllElements()
        portsList.forEach { addElement(it.descriptivePortName) }
    }

    portsJComboBox.selectedIndex = -1
    selectedIndex = -1

    portsJComboBox.addActionListener(portsSelectedListener)
}

fun showSelectPortError() {
    Toast.showToast(jPanel, "select port first")
}

fun showPortNotOpendError() {
    Toast.showToast(jPanel, "can't open this port")
}

fun removeAllActionsListeners(jComboBox: JComboBox<String>) {
    jComboBox.actionListeners.forEach { jComboBox.removeActionListener(it) }
}
