#define BAUD_RATE 115200

#define NUMBER_OF_MOTORS 4
#define NUMBER_OF_MOTOR_PINS 2

#define IMMEDIATELY_STOP_SWITCHES_COUNT 4
#define IMMEDIATELY_STOP_SWITCHES_DELAY 20

#define STATUS_MOTOR_STOP 0
#define STATUS_MOTOR_FORWARD 1
#define STATUS_MOTOR_BACKWARD -1

#define MOTOR_0_LEGS {5, 6}
#define MOTOR_1_LEGS {7, 8}
#define MOTOR_2_LEGS {9, 10}
#define MOTOR_3_LEGS {11, 12}

#define SERIAL_LED_PIN 13

void setMotorStatus(int a, int b, char code);
int getStatusCode(bool isOn, bool foreword);
void setMotorDirection(int index, int dir);
void setSerialLED(bool isOn);

int motorsLegs[NUMBER_OF_MOTORS][NUMBER_OF_MOTOR_PINS] = {MOTOR_0_LEGS, MOTOR_1_LEGS, MOTOR_2_LEGS, MOTOR_3_LEGS};
int motorsStatus[NUMBER_OF_MOTORS] = {STATUS_MOTOR_STOP, STATUS_MOTOR_STOP, STATUS_MOTOR_STOP, STATUS_MOTOR_STOP};

void setup() {
  // set serial speed
  Serial.begin(BAUD_RATE);

  // init motors legs as output
  for (int i = 0; i < NUMBER_OF_MOTORS; i++) {
    for (int j = 0; j < NUMBER_OF_MOTOR_PINS; j++) {
      pinMode(motorsLegs[i][j], OUTPUT);
    }

    // stop the motor
    setMotorStatus(i, STATUS_MOTOR_STOP);
  }

  pinMode(SERIAL_LED_PIN, OUTPUT);
}

void loop() {
  if (Serial.available() > 0) {
    // get the data form the serial port
    int data = Serial.read();

    setSerialLED(true);

    // for each motor read the new motor status form the data received and set the new status
    for (int i = 0; i < NUMBER_OF_MOTORS; i++) {
      setMotorStatus(i, getStatusCode(data & (0b00000001 << i * 2), data & (0b00000010 << i * 2)));
    }

    setSerialLED(false);
  }
}

int getStatusCode(bool isOn, bool foreword) {
  if (!isOn) return STATUS_MOTOR_STOP;
  else if (foreword) return STATUS_MOTOR_FORWARD;
  else return STATUS_MOTOR_BACKWARD;
}

void setMotorDirection(int index, int dir) {
    int pinA = motorsLegs[index][0];
    int pinB = motorsLegs[index][1];

    digitalWrite(pinA, dir == STATUS_MOTOR_FORWARD ? HIGH : LOW);
    digitalWrite(pinB, dir == STATUS_MOTOR_BACKWARD ? HIGH : LOW);
}

void setMotorStatus(int index, int statusCode) {
  // check if the motors already in this status
  if (motorsStatus[index] == statusCode) return;

  // check if needs to power off immediately
  if (statusCode == STATUS_MOTOR_STOP) {
    // this loop lock the dc motor and stop it immediately
    for (int i = 0; i < IMMEDIATELY_STOP_SWITCHES_COUNT; i++) {
      setMotorDirection(index, i % 2 == 0 ? -motorsStatus[index]: motorsStatus[index]);
      delay(IMMEDIATELY_STOP_SWITCHES_DELAY);
    }
  }

  // set motor direction
  setMotorDirection(index, statusCode);

  // update motor status
  motorsStatus[index] = statusCode;
}

void setSerialLED(bool isOn) {
    digitalWrite(SERIAL_LED_PIN, isOn ? HIGH : LOW);
}
